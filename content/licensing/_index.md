---
title: "Licensing information"
---

## [CC0 1.0 Universal][cc0]

[cc0]: LICENSE_CC0.txt

* `0001-about/1.jpg` -
  [source](https://www.publicdomainpictures.net/en/view-image.php?image=298717&picture=public-toilets)
* `0001-about/2.jpg` -
  [source](https://www.publicdomainpictures.net/en/view-image.php?image=105325&picture=portable-toilet)
* `0001-about/3.jpg` -
  [source](https://www.publicdomainpictures.net/en/view-image.php?image=363718&picture=portable-toilet)
* `0001-about/4.jpg` -
  [source](https://www.publicdomainpictures.net/en/view-image.php?image=49339&picture=old-toilet)
* `0001-about/5.jpg` -
  [source](https://www.publicdomainpictures.net/en/view-image.php?image=250060&picture=old-outhouse)
* `0001-about/6.jpg` -
  [source](https://www.publicdomainpictures.net/en/view-image.php?image=24942&picture=toilet)
* `0002-baako/candle.gif` -
  [source](https://commons.wikimedia.org/wiki/File:Jeeny_candle.gif)
* `0003-mfsb/wallpaper.jpg` -
  [source](https://publicdomainpictures.net/en/view-image.php?image=45009&picture=damask-background-brown-orange)
* `0003-mfsb/painting.jpg` -
  [source](https://commons.wikimedia.org/wiki/File:Stranover,_Tobias_-_Still-Life_with_Flowers_-_Google_Art_Project.jpg),
  [source](https://publicdomainpictures.net/en/view-image.php?image=270264&picture=framed-vintage-painting)
* `0012-framebuffer/test.png` -
  [source](https://commons.wikimedia.org/wiki/File:Chinese_HDTV_test_card.png)
* Everything in `images/cursors/`

## [Creative Commons Attribution 3.0 Unported Public License][cc-by-3]

[cc-by-3]: LICENSE_CC_BY_3.txt

* `0029-word-chain/words.txt` -
  [source](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt)
  > (EFF's long word list (for use with five dice)) Copyright 2016
  > Electronic Frontier Foundation

## [Creative Commons Attribution-ShareAlike 2.0 Generic Public License][cc-by-sa-2]

[cc-by-sa-2]: LICENSE_CC_BY_SA_2.html

* `0002-baako/ian-mcdiarmid.jpg` -
  [source](https://www.flickr.com/photos/miguel_discart/49680061466/)
  > (2020-02-22\_12-02-35\_ILCE-6500\_DSC09086\_DxO) Copyright 2020
  > Miguel Discart

## [SIL Open Font License][ofl]

[ofl]: LICENSE_OFL.txt

* `fonts/comic-neue.woff2` -
  [source](https://github.com/crozynski/comicneue/blob/380f5e82c13d85c56fd0a78df58d06343cc1393b/Fonts/WebFonts/woff2/ComicNeue/ComicNeue-Regular.woff2)
  > Copyright 2014 The Comic Neue Project Authors
  > (https://github.com/crozynski/comicneue)
* `fonts/courier-prime.woff2` -
  [source](https://github.com/quoteunquoteapps/CourierPrime/blob/7fd585a2dd4c1612c79b3308e300923d1c13df93/fonts/ttf/CourierPrime-Regular.ttf)
  > Copyright 2015 The Courier Prime Project Authors
  > (https://github.com/quoteunquoteapps/CourierPrime)
* `fonts/source-{code,sans,serif}-pro.woff2` -
  [source](https://github.com/adobe-fonts/source-code-pro/blob/235b72fc43a46cacf36e7c9b45d8d4fc0d121099/WOFF2/OTF/SourceCodePro-Regular.otf.woff2),
  [source](https://github.com/adobe-fonts/source-sans-pro/blob/4bdf42c690a214a0f69410d71a6b889c5c4a695f/WOFF2/OTF/SourceSansPro-Regular.otf.woff2),
  [source](https://github.com/adobe-fonts/source-serif-pro/blob/c811345609ee81ddb83ac707c15f7defd6269963/WOFF2/OTF/SourceSerifPro-Regular.otf.woff2)
  > Copyright 2010-2019 Adobe (<http://www.adobe.com/>), with Reserved
  > Font Name 'Source'. All Rights Reserved. Source is a trademark of
  > Adobe in the United States and/or other countries.

## [Creative Commons Attribution 4.0 International Public License][cc-by-4]

[cc-by-4]: LICENSE.txt

* `0002-baako/village.jpg` -
  [source](https://commons.wikimedia.org/wiki/File:Village_of_Rhodes,_South_Africa.jpg),
  [source](https://commons.wikimedia.org/wiki/File:American_pigs_in_blankets.jpg)
  > Original images:
  >
  > * (Rhodes) Copyright 2011 SA-Venues.com
  > * (Pigs in Blankets) Copyright 2005 stef yau
  >
  > This modified image: Copyright 2020 Bruh, Ltd
* `0004-www/stars.png` -
  [source](https://commons.wikimedia.org/wiki/File:Phoenix_dwarf_galaxy.jpg)
  > Copyright 2011 ESA/Hubble
* All other files except embedded ones
  > Copyright 2020 Bruh, Ltd
